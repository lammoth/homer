
/**
 * MongoDB Setting schema
 */

var mongoose = require('mongoose');
var _ = require('underscore');

var settingSchema = new mongoose.Schema({
    modules: [
        {
            name: String,
            enabled: Boolean,
            position: Number,
            order: Number,
            public: Boolean,
            anonymous: Boolean
        }
    ],
    themes: [
        {
            name: String,
            enabled: Boolean
        }
    ],
    logo: String
});

module.exports = settingSchema;

settingSchema.statics.exists = function(next) {
    this.findOne()
    .exec(function(err, result) {
        
        if (err) {
            return next(err);
        }

        return next(result ? true : false);
    });
};

settingSchema.statics.getModules = function(enabled, req, next) {
    this.findOne()
    .select('modules')
    .exec(function(err, result) {

        var modules = [];
        
        if (err) {
            return next(err);
        }

        if (result) {
            var modules_length = result.modules.length;
            if (enabled) {
                for (var index = 0; index < modules_length; index ++) {
                    if (result.modules[index].enabled) {
                        modules.push(result.modules[index]);
                    }
                }
            } else {
                for (var index = 0; index < modules_length; index ++) {
                    modules.push(result.modules[index]);
                }
            }
        }
        if (req) {
            req.objects = modules;
            return next();
        } else {
            return next(modules);
        }
    });
};

settingSchema.statics.addModule = function(module, next) {
    this.findOne({}, function (err, result){
        if (err) {
            return next(err);
        }

        if (result) {
            result.modules.push(
                {
                    name: module,
                    enabled: false,
                    position: null,
                    order: null
                }
            );
            result.markModified('modules');
            result.save();
            return next(result);
        }
    });
};

settingSchema.statics.updateModules = function(req, next) {
    this.findOne({}, function (err, result){
        
        var modules = [];
        
        if (err) {
            return next(err);
        }

        if (result) {

            if (req.body.module) {
                if (result.modules) {
                    var modules_length = result.modules.length;
                    for (var index = 0; index < modules_length; index ++) {
                        if (result.modules[index].name == req.body.module) {
                            var changes = false;
                            for (var property in req.body) {
                                if (_.property(result.modules[index], property)) {
                                    result.modules[index][property] = req.body[property];
                                    changes = true;
                                }
                            }
                            if (changes) {
                                // With this "tells" to Mongoose that the value has changed
                                result.markModified('modules');
                                result.save();
                            }
                        }
                    }
                }
            }
        }
        return next();
    });
};