
/**
 * MongoDB Message schema
 */

var mongoose = require('mongoose');

var messageSchema = new mongoose.Schema({
    user_src: mongoose.Schema.Types.ObjectId,
    user_dst: mongoose.Schema.Types.ObjectId,
    text: String,
    created: Date
});

module.exports = messageSchema;