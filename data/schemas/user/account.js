
/**
 * MongoDB Account schema
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    firstName: String,
    lastName: Date,
    email: String,
    roles: [],
    image: String
});

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);