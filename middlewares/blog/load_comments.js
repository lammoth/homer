
/**
 * Get posts
 */

var rek = require('rekuire');
var commentModel = rek('data/models/blog/comment');

function loadComments(model) {
    return function loadComments(req, res, next) {
        if (model)
            model.getComments(req, next);
        else
            commentModel.getComments(req, next);
    };
}

module.exports = loadComments;