
/**
 * Get platform modules
 */

var validator = require('validator');

function formValidation(data, cb) {
    function isObjectId(str){
        if (validator.isHexadecimal(str)) {
            if (str.length === 24)
                return true;
            else
                return false;
        } else {
            return false;
        }
    }
    validator.extend('isObjectId', function (str) {
        return isObjectId(str);
    });

    var errors = [];
    for (var index in data){
        switch(data[index].type)
        {
            case 'color':
                if (!validator.isHexColor(data[index].value))
                    errors.push({
                        name: index,
                        message: res.__('Must be a hex color')}
                    );
                break;
            case 'date':
                if (!validator.isDate(data[index].value))
                    errors.push({
                        name: index,
                        message: res.__('Must be a date')}
                    );
                break;
            case 'objectid':
                if (!validator.isObjectId(data[index].value))
                    errors.push({
                        name: index,
                        message: res.__('Must be a ObjectId')}
                    );
                break;
            default:
                validator.toString(data[index].value);
        }
    }

    if (errors.length > 0){
        console.log(errors);
        cb({errors: errors});

    } else {
        return cb();
    }

}

module.exports = formValidation;