
/**
 * Socket.io events manager.
 */

var rek = require('rekuire');
var loadSocketioEvents = rek('libs/load_socketio_events');

var passportSocketIo = require("passport.socketio"),
    cookieParser = require('cookie-parser'),
    settings = rek('/settings');


module.exports = function(server, app) {
    var io = require('socket.io').listen(server);

    //io.sockets.on('connection', function (socket) {
    io.on('connection', function (socket) {
        // Loading core events

        // Loading modules events
        loadSocketioEvents(socket, io, app);
    });
};