/**
 * Index routes.
 */

var passport = require("passport");
var rek = require('rekuire');
var loadPostsMiddleware = rek('middlewares/blog/load_posts');
var loadHighlightsMiddleware = rek('middlewares/load_highlights');
var modulesMiddleware = rek('middlewares/admin/modules_info');

module.exports = {
    // Default routes
    '/': {
        methods: ['get'],
        middleware: [],
        fn: function(req, res, next) {
            res.render('index');
        }
    },

    '/widget': {
        methods: ['get'],
        middleware: [],
        fn: function(req, res, next) {
            res.render('widget_view');
        }
    },

    '/widget/launch/:id/:type': {
        methods: ['get'],
        middleware: [],
        fn: function(req, res, next) {
            var id = req.param('id');
            var type = req.param('type');
            var width = req.param('w');
            var height = req.param('h');

            res.header('Content-Type', 'application/x-javascript');



            res.send('document.write("<iframe id=\\\"iframe_' + id + '\\\" name=\\\"iframe_' + id + '\\\" src=\\\"http://easyod.com/widget#/api/widget/' + id + '/' + type +  '\\\" width=\\\"' + width + '\\\" height=\\\"' + height + '\\\" frameborder=\\\"0\\\" scrolling=\\\"no\\\" style=\\\"margin-bottom: 10px\\\"></iframe>");');
        }
    },

    // Highlights routes
    '/highlights': {
        methods: ['get'],
        middleware: [modulesMiddleware(true), loadHighlightsMiddleware],
        fn: function(req, res, next) {
            console.log(req.highlights_left);
            console.log(req.highlights_right);
            res.render(
                'highlights',
                {
                    highlights_left: req.highlights_left,
                    highlights_right: req.highlights_right
                }
            );
        }
    }

};