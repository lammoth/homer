/**
 * API Index methods
 */

var settings = require('../../settings');

var routes = {};

routes[settings.apiPrefix + '/info'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.json(
            {
                'response': 'successful',
                'user': req.user
            }
        );
    }
};

routes[settings.apiPrefix + '/perms'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.json(
            {
                'response': 'successful',
                'perms': req.user.roles
            }
        );
    }
};

module.exports = routes;
