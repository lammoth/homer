/**
 * Admin Index methods
 */

var settings = require('../../settings');
var rek = require('rekuire');
var modulesMiddleware = rek('middlewares/admin/modules_info');

var routes = {};

routes[settings.siteRoutes.admin.route + '/'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.render('admin/index', {
                
        });
    }
};

routes[settings.siteRoutes.admin.route + '/modules'] =  {
    methods: ['get'],
    middleware: [modulesMiddleware(false)],
    fn: function(req, res, next) {
        res.render('admin/modules',
            {
                modules: req.objects
            }
        );
    }
};

module.exports = routes;
