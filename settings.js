/**
 * Main settings.
 */


// Node port
exports.port = 8000;

// Root path
var rootPath = process.cwd();
exports.rootPath = rootPath;

// Secret app key
exports.secret = 'P3p34rne5T0y43st4aqU1y4T0d0sv4aC0mb4t1r';

// API version
exports.apiPrefix = '/api/v1';

// Paths
// Libs path
exports.libsPath = rootPath + '/libs/';
// Middlewares path
exports.middlewaresPath = rootPath + '/middlewares/';
// // Models path
exports.modelsPath = rootPath + '/data/models/';
// // Schemas path
exports.schemasPath = rootPath + '/data/schemas/';
// // Views path
exports.viewsPath = rootPath + '/views/';
// Modules path
exports.modulesPath = rootPath + '/modules/';
// Themes path
exports.themesPath = rootPath + '/public/themes/';

// Site settings
exports.site = {
    // Site title
    title: {
        property: 'title',
        content: 'EasyOD'
    },
    // Default theme
    theme: {
        property: 'theme',
        content: 'default'
    },
    sections: {
        property: 'sections',
        content: []
    }
};

// Site static routes
exports.siteRoutes = {
    // Admin route
    admin: {
        route: '/admin'
    },
};

// Modules to load
exports.modules = [
    'opendata'
];

// Socketio events array
exports.modules_sio_events = [];

// Socketio socket
exports.sio = {
    io: null,
    socket: null
};

// Widgets
// Twitter address
exports.twitterURL = 'twitter.com';