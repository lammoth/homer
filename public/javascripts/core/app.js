/**
 * @file app.js
 * @namespace AngularJS Main App
 * AngularJS needs controllers to operate, this file create the main app.
 */

angular.module(
    'WeTalk',
    [
        'ui.router',
        //'ui.bootstrap',
        'i18n',
        'RESTservice',
        'Socketio',
        'ngDragDrop',
        'permService'
    ],
    permissionList
);

angular.module('WeTalk').run(
    function($permission) {
        $permission.setPermissions(permissionList);
    }
);

var permissionList;

angular.element(document).ready(function() {
    permissionList = [];
    angular.bootstrap(document, ['WeTalk']);
    // $.get('/api/v1/perms', function(data) {
    //     permissionList = data.perms;
    //     angular.bootstrap(document, ['WeTalk']);
    // });
});

