angular.module('WeTalk').config(
    [
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            
            $stateProvider.
                state('/admin/modules', {
                    url: '/admin/modules',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/admin/modules",
                                controller: 'AdminModulesController'
                            }
                    }
                }).
                state('/admin/dist', {
                    url: "/admin/dist",
                    views: {
                        "main_content":
                            {
                                templateUrl: "/admin/dist",
                                controller: 'AdminDistController'
                            }
                    }
                });
        }
    ]
);