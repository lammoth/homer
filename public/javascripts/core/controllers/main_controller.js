/**
 * @file main_controller.js
 * @namespace Index Controller
 * This module manage AngularJS index operations
 */

// This controller is a dummy controller.
// It's only purpose is to show the highlights of the modules
angular.module('WeTalk').controller('HighLightsController',
    [
        '$scope',
        function ($scope) {

        }
    ]
);