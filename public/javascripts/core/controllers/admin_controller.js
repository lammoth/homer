/**
 * @file admin_controller.js
 * @namespace Admin Controller
 * @desc This module manage AngularJS admin operations
 */

/**
  * @desc  Toggle module state
  * @param bool $module - Set module to change 
  * @param bool $state - Current module state
  * @return array - Modules requested
*/
angular.module('WeTalk').controller('AdminModulesController',
    [
        '$scope',
        '$templateCache',
        'restService',
        function($scope, $templateCache, restService) {
            $scope.toggle_module = function(module, state) {
                $templateCache.removeAll();
                restService.post(
                    {
                        module: module,
                        enabled: (state ? false : true)
                    },
                    '/api/v1/admin/modules/update',
                    function(data, status, headers, config) {

                    },
                    function(data, status, headers, config) {}
                );
            };
        }
    ]
);

// This controller loads the interface needed to make a modules distribution.
angular.module('WeTalk').controller('AdminDistController',
    [
        '$scope',
        '$templateCache',
        'restService',
        function ($scope, $templateCache, restService) {

            // This array contains all modules enabled
            $scope.modules_availabled = [];

            // This array contains the right modules enabled
            $scope.modules_enabled_right = [];

            // This array contains the left modules enabled
            $scope.modules_enabled_left = [];

            // Gets all modules enabled
            // Available positions:
            // 1: Left position
            // 2: Right position
            restService.get(
                {},
                '/api/v1/admin/modules/?available=true',
                function(data, status, headers, config) {
                    if (data.modules) {
                        for (var index in data.modules) {
                            // Adding left modules
                            if (data.modules[index].position && data.modules[index].position == 1) {
                                $scope.modules_enabled_left.push(data.modules[index]);

                            // Adding right modules
                            } else if (data.modules[index].position && data.modules[index].position == 2) {
                                $scope.modules_enabled_right.push(data.modules[index]);

                            // Adding availables and not located modules
                            } else {
                                $scope.modules_availabled.push(data.modules[index]);
                            }
                        }
                    }

                    // Adding empty elements to the available arrays
                    if (data.modules) {
                        $scope.modules_enabled_left = get_empty_items($scope.modules_enabled_left, $scope.modules_enabled_left, data.modules.length);
                        $scope.modules_enabled_right = get_empty_items($scope.modules_enabled_right, $scope.modules_enabled_right, data.modules.length);
                        $scope.modules_availabled = get_empty_items($scope.modules_availabled, $scope.modules_availabled, data.modules.length);
                    }
                },
                function(data, status, headers, config) {
                    console.log(data);
                }
            );
            
            // This function adds empty items to the arrays
            function get_empty_items(src_list, dst_list, total_count) {
                if (src_list.length < total_count) {
                    var diff = total_count - src_list.length;
                    for (var index = 0; index < diff; index ++) {
                        dst_list.push({});
                    }
                }
                return _.sortBy(dst_list, 'order');
            }

            $scope.startCallback = function(event, ui, module) {
                $scope.draggedModule = module;
            };

            $scope.dropCallback = function(event, ui, module, order, position) {
                $templateCache.removeAll();
                restService.post(
                    {
                        module: module.name,
                        position: position,
                        order: order
                    },
                    '/api/v1/admin/modules/update',
                    function(data, status, headers, config) {

                    },
                    function(data, status, headers, config) {}
                );
            };

        }
    ]
);

angular.module('WeTalk').controller('AdminStyleController',
    [
        '$scope',
        'restService',
        function ($scope, restService) {
            $scope.formData = {};
            $scope.validator = function() {
                restService.post($scope.formData, '/admin/ui/customstyle',
                    function(data, status, headers, config) {
                        $scope.errors = {};
                        if (data.errors){
                            for (var index in data.errors) {
                                $scope.errors[data.errors[index].name] = data.errors[index].message;
                                
                            }
                        }
                        console.log('controller success');
                    },
                    function(data, status, headers, config) {
                        // console.log(data);
                        console.log('controller error');
                    }
                );
            };
        }
    ]
);
