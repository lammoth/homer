/**
 * @file chat_controller.js
 * @namespace Chat Controller
 * This module manage AngularJS chat operations
 */


/**
  * @desc  Chat
  * @param object $scope - The controller scope var
  * @param object restService - Rest service
  * @param object socketIO - SocketIO service
  * @param object $compile - Compile service
*/
angular.module('WeTalk').controller('ChatController',
    [
        '$scope',
        'socketIO',
        'restService',
        '$compile',
        function ($scope, socketIO, restService, $compile) {

            // Get User from request
            restService.get({}, 'api/v1/info',
                function(data, status, headers, config) {
                    $scope.user = data.user;

                    if($scope.user) {
                        socketIO.emit('userConnect', $scope.user._id);
                    }
                    // Open chat window
                    $scope.chatVisible = true;
                },
                function(data, status, headers, config) {
                    console.log('controller error');
                }
            );
            

            // First scroll chat bottom
            var content_el = document.getElementById('chat-content');
            content_el.scrollTop = content_el.scrollHeight;



            // Close chat window
            $scope.close = function(){
                $scope.chatVisible = false;
            };

            // Comunicate to server new message
            $scope.send = function(){
                if ($scope.user) {
                    var text = $scope.chatText;
                    $scope.chatText = "";
                    $scope.chat_el = angular.element('#chat-content');
                    // $scope.room = $scope.chat_el.attr('room');
                    
                    var data = {
                        room: $scope.room,
                        user_id: $scope.user._id,
                        user_name: $scope.user.username,
                        image: $scope.user.image,
                        message: text
                    };
                    socketIO.emit('sendChatMessage', data);
                }
            };

            // Open a specific user chat window when clik at user image.
            $scope.chatWith = function(element){
                if ($scope.user) {
                    $scope.chat_el = angular.element('#chat-content');
                    angular.element('#chat-users li').removeClass('active');
                    var user_active = angular.element(element).addClass('active');
                    

                    if(user_active.attr('room')) {
                        // Clear badge
                        user_active.find('span.badge').remove();

                        $scope.room = user_active.attr('room');
                        $scope.chat_el.find('.room').css('display', 'none');
                        var room_el = angular.element($scope.chat_el.find('.room[room="'+$scope.room+'"]'));
                        room_el.css('display', 'inherit');
                    } else {
                        var me = $scope.socket_id;
                        var other = angular.element(element).attr('uid');
                        var roomData = {name: me+'@'+other, users:[me, other]};
                        socketIO.emit('newChatRoom', roomData);
                        $scope.room = me+'@'+other;
                    }

                    angular.element('#chat-text > input').focus();
                    angular.element('#chat-header .chat-name').html(user_active.find('span').text());
                    content_el.scrollTop = content_el.scrollHeight;

                }
            };

            // When someone want to chat with me.
            socketIO.on('needSuscription', function (data) {
                if ($scope.user){

                    // When message is received
                    socketIO.on(data.name, function(data){
                        $scope.chat_el = angular.element('#chat-content');
                        var room_el = angular.element($scope.chat_el.find('.room[room="'+data.room+'"]'));
                        var el_class = null;
                        if ($scope.user._id == data.user_id)
                            el_class = "chat-message-me";
                        else
                            el_class = "chat-message-other";
            

                        childScope = $scope.$new();
                        childScope.message = {
                            el_class: el_class,
                            username: data.user_name,
                            message: data.message,
                            image: data.image
                        };

                        room_el.append($compile('<message>')(childScope));

                        content_el.scrollTop = content_el.scrollHeight;
                        
                        if($scope.user._id != data.user_id && $scope.room != data.room) {
                            var user_el = angular.element('#chat-users > .chat-users').find('li[room="'+data.room+'"]');
                            var n_message = parseInt(user_el.find('span.badge').text());
                            if (isNaN(n_message)) {
                                n_message = 1;
                                user_el.append('<span class="badge">'+n_message+'</span>');
                            } else {
                                n_message++;
                                user_el.find('span.badge').text(n_message);
                            }
                        }
                    });


                    for(var user in data.uid){
                        var user_el = angular.element('#chat-users li[uid="'+data.uid[user]+'"]');
                        user_el.attr('room', data.name);
                        if(user_el.attr('room') == data.name){
                            var room_el = angular.element('<div class="room" room="'+data.name+'"></div>');
                            $scope.chat_el = angular.element('#chat-content');
                            $scope.chat_el.append(room_el);

                            for (var i in data.lastMessages) {
                                room_el.prepend(data.lastMessages[i].text + '<br>');
                            }
                            content_el.scrollTop = content_el.scrollHeight;
                        } else {
                            angular.element('#chat-content .room[room="'+data.name+'"]').css('display', 'none');
                        }
                    }
                }
            });

            // It's called when I connect to chat.
            // Tell me the users that are online.
            socketIO.on('usersConnected', function (users) {
                if ($scope.user){
                    $scope.users_el = angular.element('#chat-users .chat-users');
                    for(var index in users){
                        var user = users[index];
                        if (user._id != $scope.user._id){
                            var el = angular.element('<li class="chat-user" uid="'+user._id+'" ng-click="chatWith($event.currentTarget)"><img class="img-circle" src="'+user.image+'"><br><span>'+user.username+'</span></li>');
                            $scope.users_el.append($compile(el)($scope));
                        } else {
                            $scope.socket_id = user._id;
                        }
                    }
                }
            });

            // It's called when a user has been connected to chat.
            socketIO.on('userConnected', function (user) {
                if ($scope.user){
                    $scope.users_el = angular.element('#chat-users .chat-users');
                    if (user._id != $scope.user._id && !$scope.users_el.find('li[uid="'+user._id+'"]').attr('uid')){
                        var el = angular.element('<li class="chat-user" uid="'+user._id+'" ng-click="chatWith($event.currentTarget)"><img class="img-circle" src="'+user.image+'"><br><span>'+user.username+'</span></li>');
                        $scope.users_el.append($compile(el)($scope));
                    }
                }
            });

            // It's called when a user has been disconnected.
            socketIO.on('userDisconnected', function (user) {
                if ($scope.user){
                    $scope.users_el = angular.element('#chat-users .chat-users');
                    var user_el = $scope.users_el.find('.chat-user[uid="'+user+'"]');

                    var room_id = user_el.attr('room');
                    var room_el = angular.element('#chat-container').find('.room[room="'+room_id+'"]');
                    
                    room_el.remove();
                    user_el.remove();
                }
            });

        }
    ]
);

// Directive for send message when enter is pressed.
angular.module('WeTalk').directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                event.preventDefault();
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
            }
        });
    };

});
angular.module('WeTalk').directive('message', function() {
    return {
        restrict: 'E',
        replace: true,
        templateUrl: '/chat/partials/chat_message'
    };
});