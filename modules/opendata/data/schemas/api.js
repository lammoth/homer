
/**
 * MongoDB Data schema
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    ObjectId = require("mongoose").Types.ObjectId;

var apiSchema = new Schema({
    sheets: [],
    name: String,
    description: String,
    created: Date
});

apiSchema.statics.getObjects = function(req, next) {
    var params = {};

    if (_.isEmpty(req.query) && !_.isEmpty(req.params)) {
        params['_id'] = ObjectId(req.params.id);
    } else if (!_.isUndefined(req.query)) {
        params = req.query;
    }

    this.find((params ? params : null))
        .lean()
        .select(params ? '' : 'name description created')
        .sort({'created': -1})
        .limit(params ? '' : 100)
        .exec(function(err, objects) {
            if (err) {
                return next(err);
            }
            if (params['_id']) {
                req.objects = _.first(objects);
            } else {
                req.objects = objects;
            }
            return next();
        });
};

module.exports = mongoose.model('Api', apiSchema);