
/**
 * MongoDB Data schema
 */

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    _ = require('underscore'),
    ObjectId = require("mongoose").Types.ObjectId;

var dataSchema = new Schema({
    data: [],
    name: String,
    // name: { type: String, index: true },
    file: String,
    source: String,
    created: Date,
});

dataSchema.index({ name: 1, type: -1 });
dataSchema.set('autoIndex', true);

dataSchema.statics.getObjects = function(req, next) {
    var params = {};

    if (_.isEmpty(req.query) && !_.isEmpty(req.params)) {
        params['_id'] = ObjectId(req.params.id);
    } else if (!_.isUndefined(req.query)) {
        params = req.query;
    }

    this.find((params ? params : null))
        .lean()
        .select(params ? '' : 'file source created')
        .sort({'created': -1})
        .exec(function(err, objects) {
            if (err) {
                return next(err);
            }
            if (params['_id']) {
                req.objects = _.first(objects);
            } else {
                req.objects = objects;
            }
            return next();
        });
};

module.exports = mongoose.model('Data', dataSchema);