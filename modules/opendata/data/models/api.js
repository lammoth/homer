/**
 * Data model
 */

var mongoose = require('mongoose');
var rek = require('rekuire');
var apiSchema = rek('modules/opendata/data/schemas/api');

var api = mongoose.model('Api', apiSchema);
module.exports = api;