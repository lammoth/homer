/**
 * Data model
 */

var mongoose = require('mongoose');
var rek = require('rekuire');
var dataSchema = rek('modules/opendata/data/schemas/data');

var data = mongoose.model('Data', dataSchema);
module.exports = data;