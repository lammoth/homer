/**
 * Index routes.
 */

var passport = require("passport");
var rek = require('rekuire');
var loadPostsMiddleware = rek('middlewares/blog/load_posts');
var loadHighlightsMiddleware = rek('middlewares/load_highlights');
var modulesMiddleware = rek('middlewares/admin/modules_info');


var _ = require('underscore');
var parse = require('csv-parse');
var modelData = rek('modules/opendata/data/models/data');
var modelAPI = rek('modules/opendata/data/models/api');
var parserFile = rek('modules/opendata/middlewares/parser_file');
var getRawData = rek('modules/opendata/middlewares/get_raw_data');
var saveAPI = rek('modules/opendata/middlewares/create_api');
var getAPIData = rek('modules/opendata/middlewares/get_api_data');


var rek = require('rekuire');
var path = require('path');
var m_settings_path = path.join(__dirname, '../settings');
var m_settings = require(m_settings_path);

var routes = {};

routes['/' + m_settings.route_prefix + '/partials/:name'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        var name = req.params.name;
        res.render(m_settings.viewsPath + 'partials/' + name);
    }
};

routes['/' + m_settings.route_prefix + '/launch'] = {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.render(m_settings.viewsPath + 'launch/sliders');
    }
};

routes['/' + m_settings.route_prefix + '/parse'] =  [
    {
        methods: ['get'],
        middleware: [getRawData(modelData)],
        fn: function(req, res, next) {
            res.render(m_settings.viewsPath + 'parse', {
                raw_data: req.objects
            });
        }
    },
    {
        methods: ['post'],
        middleware: [parserFile()],
        fn: function(req, res, next) {
            res.json({
                success: true,
            });
        }
    }
];

routes['/' + m_settings.route_prefix + '/raw/detail'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.render(m_settings.viewsPath + 'data_grid');
    }
};

routes['/' + m_settings.route_prefix + '/raw/:id/detail'] =  {
    methods: ['get'],
    middleware: [getRawData(modelData)],
    fn: function(req, res, next) {
        res.json({
            data: req.objects.data
        });
    }
};

routes['/' + m_settings.route_prefix + '/api/list'] =  {
    methods: ['get'],
    middleware: [getAPIData(modelAPI)],
    fn: function(req, res, next) {
        res.render(m_settings.viewsPath + 'api_list', {
            apis: req.objects
        });
    }
};

routes['/' + m_settings.route_prefix + '/api/create'] =  {
    methods: ['post'],
    middleware: [saveAPI(modelAPI)],
    fn: function(req, res, next) {
        res.json({
            success: true
        });
    }
};

routes['/' + m_settings.route_prefix + '/api/graph/selector'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.render(m_settings.viewsPath + 'graph_selector');
    }
};

routes['/' + m_settings.route_prefix + '/api/graph/widget'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.render(m_settings.viewsPath + 'graph_widget');
    }
};

routes['/' + m_settings.route_prefix + '/api/graph/representation'] =  {
    methods: ['get'],
    middleware: [],
    fn: function(req, res, next) {
        res.render(m_settings.viewsPath + 'graph_representation');
    }
};

routes['/' + m_settings.route_prefix + '/api'] =  {
    methods: ['get'],
    middleware: [getAPIData(modelAPI)],
    fn: function(req, res, next) {
        res.json({
            data: req.objects
        });
    }
};

routes['/' + m_settings.route_prefix + '/api/:id'] =  {
    methods: ['get'],
    middleware: [getAPIData(modelAPI)],
    fn: function(req, res, next) {
        res.json({
            data: req.objects
        });
    }
};


module.exports = routes;

