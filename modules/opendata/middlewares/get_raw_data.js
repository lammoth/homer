
/**
 * Get objects
 */

var rek = require('rekuire');

function loadObjects(model) {
    return function loadObjects(req, res, next) {
        model.getObjects(req, next);
    };
}

module.exports = loadObjects;