
/**
 * Save an element
 */

var _ = require('underscore');
var rek = require('rekuire');
var createOperation = rek('libs/crud_operations/create');
var mainSettings = require('../../../settings');

function createObject(model, token) {
    sio_data = {
        call: mainSettings.sio,
        name: 'apiSaved',
    };

    return function createObject(req, res, next) {
        data = req.body;
        createOperation(model, data, function() {
            sio_data.call.io.sockets.emit(sio_data.name, {token: data.token});
            next();
        });
    };
}

module.exports = createObject;
