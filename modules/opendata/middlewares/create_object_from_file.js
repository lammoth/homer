
/**
 * Save an element
 */

var _ = require('underscore');
var rek = require('rekuire');
var createOperation = rek('modules/opendata/libs/crud_operations/create_raw');

function createObject(model, data, sio) {
    createOperation(model, data, sio);
}

module.exports = createObject;
