
/**
 * Save an element
 */

var _ = require('underscore');
var rek = require('rekuire');
var path = require('path');
var uuid = require('node-uuid');
var http = require('http');
var fs = require('fs');
var XLS = require('xlsjs');
var Q = require('q');
var rootPath = process.cwd() + '/public/raw_data/';
var createObjectData = rek('modules/opendata/middlewares/create_object_from_file');
var dataModel = rek('modules/opendata/data/models/data');
var mainSettings = require('../../../settings');


function parserFile() {
    function makeRequest(url, token) {
        var name = uuid.v4();
        var file_path = rootPath + name + '.xls';
        var file = fs.createWriteStream(file_path);
        var deferred = Q.defer();
        var extname = path.extname(url);

        file.on('open', function () {
            http.get(url, function (response) {
                response.on('error', function (err) {
                    deferred.reject(err);
                });
                response.pipe(file);
            });
        }).on('error', function (err) {
            deferred.reject(err);
        }).on('finish', function () {
            deferred.resolve(file_path);
            deferred.promise.then(function(f) {
                var sheet_data = null;
                switch(extname) {
                    case '.xls':
                        sheet_data = parserXLSFile(file_path, name, url, token);
                        sio_data = {
                            call: mainSettings.sio,
                            name: 'urlAdded',
                            data: {'url': sheet_data.source, 'token': token}
                        };
                        createObjectData(dataModel, sheet_data, sio_data);
                        break;
                    default:
                        break;
                }
            });
        });
    }

    function parserXLSFile(file, name, url, token) {
        var sheet_data = [];
        var xls = XLS.readFile(file);
        _.each(xls.SheetNames, function(sheetname) {
            var sheet_json = XLS.utils.sheet_to_json(xls.Sheets[sheetname]);
            sheet_data.push({sheet: sheetname, raw: sheet_json});
        });
        return {
                data: sheet_data,
                name: name,
                source: url,
                file: _.last(url.split('/')),
                created: new Date()
        };
    }

    return function parserFile(req, res, next) {
        makeRequest(req.body.url, req.body.token);
        next();
    };
}

module.exports = parserFile;
