/**
 * @file main_controller.js
 * @namespace Index Controller
 * This module manage AngularJS index operations
 */

angular.module('WeTalk').controller('ODMainController',
    [
        '$scope',
        '$state',
        'restService',
        'socketIO',
        '$templateCache',
        function ($scope, $state, restService, socketIO, $templateCache) {

            $scope.parsefield = null;
            $scope.token = null;

            $scope.apis = [];

            // Get last APIs
            restService.get(
                {},
                '/opendata/api',
                function(data, status, headers, config) {
                    _.each(data.data, function(api) {
                        if (api.sheets[0].data[0].fields.length === 2) {
                            api['src'] = "http://easyod.com/widget/#/api/widget/" + api._id + "/" + 2;
                            $scope.apis.push(api);
                        }
                    });
                },
                function(data, status, headers, config) {}
            );

            // Listening socket to check if the URL has been parsed
            socketIO.on('urlAdded', function (data) {
                if (data.url == $scope.parsefield && data.token == $scope.token) {
                    $templateCache.removeAll();
                    $state.transitionTo('raw_list', {}, {reload: true});
                }
            });

            var circle = new Sonic({
                width: 30,
                height: 30,

                stepsPerFrame: 3,
                trailLength: 1,
                pointDistance: 0.01,
                fps: 30,
                step: 'fader',

                strokeColor: '#FFF',

                setup: function() {
                    this._.lineWidth = 6;
                },

                path: [
                    ['arc', 15, 15, 10, 360, 0]
                ]
            });

            $scope.parse = function() {
                $scope.token = Math.random().toString(36).substr(2);
                
                circle.play();
                $('.parse-btn').html(circle.canvas);
                $('.parse-btn').prop('disabled', true);

                restService.post(
                    {
                        url: $scope.parsefield,
                        token: $scope.token
                    },
                    '/opendata/parse',
                    function(data, status, headers, config) {},
                    function(data, status, headers, config) {}
                );
            };
        }
    ]
);

angular.module('WeTalk').controller('ODRawListController',
    [
        '$scope',
        '$stateParams',
        '$templateCache',
        'restService',
        '$state',
        function ($scope, $stateParams, $templateCache, restService, $state) {
            $scope.isShown = function(object, value) {
                $scope[object] = !value;
            };
        }
    ]
);

angular.module('WeTalk').controller('ODRawController',
    [
        '$scope',
        '$state',
        '$stateParams',
        'restService',
        'socketIO',
        '$templateCache',
        function ($scope, $state, $stateParams, restService, socketIO, $templateCache) {
            var ng = $('#navbar-grid');

            window.onscroll = function() {
                var scroll = window.scrollY;
                var top;
                if (scroll < 50)
                 top = 50 - scroll;
                else
                 top = 0;
                ng.css('top', top);
            };

            restService.get(
                {},
                '/opendata/raw/' + $stateParams.id + '/detail',
                function(data, status, headers, config) {
                    $scope.grid = data;
                    $('#loading').remove();
                },
                function(data, status, headers, config) {}
            );

            $scope.visible_input = function(n) {
                $scope.show_inputs = n;
            };

            $scope.check = function(el) {
                // TODO: use angular
                $(el).closest('.panel-body').find('input[type="checkbox"]').attr('checked', true);
            };

            $scope.colorize = function() {
                $('#accordion').find('.colored').removeClass('colored');
                $('#accordion').find('input[type="checkbox"].check_col:checked').each(function(){
                    var this_e = $(this);
                    _.each($scope.inputs, function(col) {
                        var n = parseInt(col) + 1;
                        $(this_e).closest('tr').find('td:eq('+n+')').addClass('colored');
                    });
                });
            };

            $scope.check_col = function(el) {
                var old = $(el).prev('input').is(':checked');
                var cl1 = 'glyphicon-check';
                var cl2 = 'glyphicon-unchecked';
                $(el).prev('input').prop('checked', !old);
                if (old)
                    $(el).removeClass(cl1).addClass(cl2);
                else
                    $(el).removeClass(cl2).addClass(cl1);
                $scope.colorize();
            };

            $scope.check_row = function(el) {
                var index = $(el).parent().attr('index');
                var old = $(el).prev('input').is(':checked');
                $(el).prev('input').prop('checked', !old);
                var check = $(el).prev('input').is(':checked');
                $('#accordion').find('thead > tr > td[index="' + index + '"] > input[type="checkbox"]').attr('checked', check);

                if (!_.has($scope, 'inputs')) {
                    $scope['inputs'] = [];
                }

                if (check) {
                    $(el).addClass('active');
                    $scope.inputs.push(index);
                } else {
                    $(el).removeClass('active');
                    var to_delete = null;
                    _.each($scope.inputs, function(inpt, i) {
                        if (inpt == index)
                            to_delete = i;
                    });
                    if (!_.isNull(to_delete)) {
                        $scope.inputs.splice(to_delete, 1);
                    }
                }

                if ($scope.inputs.length > 0)
                    if (check)
                        $scope.show_inputs = index;
                    else
                        $scope.show_inputs = _.last($scope.inputs);
                else
                    $scope.show_inputs = false;
                $scope.colorize();
            };

            $scope.token = null;

            var circle = new Sonic({
                width: 15,
                height: 15,

                stepsPerFrame: 3,
                trailLength: 1,
                pointDistance: 0.01,
                fps: 30,
                step: 'fader',

                strokeColor: '#FFF',

                setup: function() {
                    this._.lineWidth = 4;
                },

                path: [
                    ['arc', 7.5, 7.5, 5, 360, 0]
                ]
            });

            // Listening socket to check if the API has been saved
            // socketIO.on('apiSaved', function (data) {
            //     if (data.token == $scope.token) {
            //         $('#api_modal').modal('toggle');
            //         setTimeout(
            //             function() {
            //                 $templateCache.removeAll();
            //                 $state.transitionTo('api_list', {}, {reload: true});
            //             },
            //             1000
            //         );
            //     }
            // });

            $scope.openModal = function() {
                $('#api_modal').modal('toggle');
            };


            $scope.save = function() {
                $scope.token = Math.random().toString(36).substr(2);
                circle.play();
                $('.save-api-btn').html(circle.canvas);
                $('.save-api-btn').prop('disabled', true);

                var name = $('#api_name').val();
                var desc = $('#api_desc').val();

                var sheets = [];

                $('#accordion').find('.sheet').each(function() {
                    // Each sheet
                    if ($(this).find('input.check_row[type="checkbox"]:checked').size() > 0) {
                        // Creating sheet
                        var sheet_name = $(this).find('.sheet-name').text();
                        var sheet_data = [];

                        $(this).find('tbody > tr').each(function(){
                            // each data
                            // if this row is selected
                            if ($(this).find('input[type="checkbox"]').is(':checked')) {
                                var fields = [];
                                $(this).find('td').each(function() {
                                    // each field
                                    var this_n_col = $(this).attr('index');
                                    if ($(this).closest('table').find('thead > tr > td[index="' + this_n_col + '"] > input[type="checkbox"]').is(':checked')) {
                                        var field = null;
                                        var field_type = $('#inputs').find('form[inpt="' + this_n_col + '"] .type').val();
                                        if (field_type == 'text') {
                                            field = {
                                                type: $('#inputs').find('form[inpt="' + this_n_col + '"] .type').val(),
                                                value: $(this).text(),
                                                name: $('#inputs').find('form[inpt="' + this_n_col + '"] .title').val()
                                            };
                                            if(!_.isUndefined(field.value) && !_.isNull(field.value))
                                                fields.push(field);
                                        } else {
                                            var value = $(this).text();
                                            value = value.replace(/"/g, "");
                                            value = parseFloat(value).toFixed(2);
                                            field = {
                                                type: $('#inputs').find('form[inpt="' + this_n_col + '"] .type').val(),
                                                value: value,
                                                name: $('#inputs').find('form[inpt="' + this_n_col + '"] .title').val()
                                            };
                                            if(!_.isNaN(field.value) && !_.isUndefined(field.value) && !_.isNull(field.value))
                                                fields.push(field);
                                        }
                                    }
                                });
                                sheet_data.push({fields: fields});
                                
                            }
                        });


                        var sheet = {
                            name: sheet_name,
                            data: sheet_data
                        };
                        sheets.push(sheet);

                    }

                });

                restService.post(
                    {
                        name: name,
                        description: desc,
                        created: new Date(),
                        sheets: sheets,
                        token: $scope.token
                    },
                    '/opendata/api/create',
                    function(data, status, headers, config) {
                        setTimeout(
                            function() {
                                $('#api_modal').modal('toggle');
                            },
                            1000
                        );
                        
                        setTimeout(
                            function() {
                                $templateCache.removeAll();
                                $state.transitionTo('api_list', {}, {reload: true});
                            },
                            1000
                        );
                    },
                    function(data, status, headers, config) {}
                );
            };

            $scope.generateGraph = function(id) {
                
            };
        }
    ]
);

angular.module('WeTalk').controller('ODApiListController',
    [
        '$scope',
        '$state',
        '$stateParams',
        '$templateCache',
        'restService',
        function ($scope, $state, $stateParams, $templateCache, restService) {
            $scope.isShown = function(object, value) {
                $scope[object] = !value;
            };

            $scope.selectGraph = function(id) {
                $state.transitionTo('api_list_selector', {id: id});
            };

            $scope.goToApi = function(id) {
                window.open('http://easyod.com/opendata/api/' + id, '_blank');
            };
        }
    ]
);

angular.module('WeTalk').controller('ODApiGraphController',
    [
        '$scope',
        '$state',
        '$stateParams',
        'restService',
        function ($scope, $state, $stateParams, restService) {
            $scope.generateGraph = function(type) {
                if ($stateParams.id) {
                    $state.transitionTo('api_list_graph', {id: $stateParams.id, type: type});
                }
            };
        }
    ]
);

angular.module('WeTalk').controller('ODApiGraphRespresentationController',
    [
        '$scope',
        '$state',
        '$stateParams',
        'restService',
        function ($scope, $state, $stateParams, restService) {
            var _min_width  = 320;
            var _max_width = 980;
            var _min_height = 450;
            var _max_height = 600;
            var _default_width = 350;
            var _default_height = 500;

            $scope.v_width = '350';
            $scope.v_height = '570';
            $scope.HTMLblock = '<script language="javascript" type="text/javascript" src="http://easyod.com/widget/launch/' + $stateParams.id + '/' + $stateParams.type + '?w=' + $scope.v_width + '&h=' + $scope.v_height + '"></script>';

            switch(parseInt($stateParams.type, 10)) {
                // Dispatching Events
                case 1:
                    showPieChart($stateParams.id);
                    break;
                case 2:
                    showDonutChart($stateParams.id);
                    break;
                case 3:
                    showBarChart($stateParams.id);
                    break;
                default:
                    break;
            }

            function showPieChart(id){
                restService.get(
                    {},
                    '/opendata/api/' + id,
                    function(data, status, headers, config) {

                        var graph_data = [];
                        $scope.graph_title = data.data.name;
                        $scope.api_description = data.data.description;

                        _.each(data.data.sheets, function(d){

                            _.each(d.data, function(f){
                                var column_value;
                                var column_name;

                                _.each(f.fields, function(c){
                                    if (c.type == 'number') {
                                        column_value = parseFloat(c.value);
                                    }else if (c.type == 'text') {
                                        column_name = c.value;
                                    }
                                });
                                graph_data.push([column_name, column_value]);
                            });
                        });

                        var chart = c3.generate({
                            data: {
                                columns: graph_data,
                                type : 'pie',
                            },
                            tooltip: {
                                format: {
                                    value: function (value, ratio, id) {
                                        var format = id === 'data1' ? d3.format(',') : d3.format('');
                                        return format(value);
                                    }
                                }
                            }
                        });
                        
                    },
                    function(data, status, headers, config) {}
                );
            }

            function showDonutChart(id){
                restService.get(
                    {},
                    '/opendata/api/' + id,
                    function(data, status, headers, config) {

                        var graph_data = [];
                        $scope.graph_title = data.data.name;
                        $scope.api_description = data.data.description;

                        _.each(data.data.sheets, function(d){

                            _.each(d.data, function(f){
                                var column_value;
                                var column_name;

                                _.each(f.fields, function(c){
                                    if (c.type == 'number') {
                                        column_value = parseFloat(c.value);
                                    }else if (c.type == 'text') {
                                        column_name = c.value;
                                    }
                                });
                                graph_data.push([column_name, column_value]);
                            });
                        });

                        var chart = c3.generate({
                            data: {
                                columns: graph_data,
                                type : 'donut',
                            },
                            tooltip: {
                                format: {
                                    value: function (value, ratio, id) {
                                        var format = id === 'data1' ? d3.format(',') : d3.format('');
                                        return format(value);
                                    }
                                }
                            }
                        });
                        
                    },
                    function(data, status, headers, config) {}
                );
            }

            function showBarChart(id){
                restService.get(
                    {},
                    '/opendata/api/' + id,
                    function(data, status, headers, config) {

                        var graph_data = [];
                        var graph_name = ['x'];
                        var bar_value_name_ok = false;
                        $scope.graph_title = data.data.name;
                        $scope.api_description = data.data.description;

                        _.each(data.data.sheets, function(d){

                            _.each(d.data, function(f){
                                var bar_value;
                                var bar_value_name;
                                var bar_name;

                                _.each(f.fields, function(c){
                                    if (c.type == 'number') {
                                        bar_value = parseFloat(c.value);
                                        bar_value_name = c.name;
                                    }else if (c.type == 'text') {
                                        bar_name = c.value;
                                    }
                                });

                                if (bar_value_name_ok == false){
                                    graph_data.push(bar_value_name);
                                    bar_value_name_ok = true;
                                }
                                graph_data.push(bar_value);
                                graph_name.push(bar_name);
                            });
                        });

                    var chart = c3.generate({
                        data: {
                            x : 'x',
                            columns: [
                                graph_name,
                                graph_data,
                            ],
                            type: 'bar'
                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: 0
                                },
                                height: 30
                            }
                        }
                    });
                },
                    function(data, status, headers, config) {}
                );
            }

            $scope.changeHtmlCode = function() {

                var width = $scope.v_width;
                var height = $scope.v_height;

                if (isNaN(height)) {
                    height = _default_height;
                    $scope.v_height = height;
                }
                else {
                    if (height <= _min_height) {
                        height = _min_height;
                        $scope.v_height = height;
                  }
                    if (height > _max_height) {
                        height = _max_height;
                        $scope.v_height = _max_height;
                  }
                }

                if (isNaN(width)) {
                    width = _default_width;
                    $scope.v_width = width;
                } else {
                    if (width <= _min_width) {
                        width = _min_width;
                        $scope.v_width = width;
                  }
                    if (width > _max_width) {
                        width = _max_width;
                        $scope.v_width = width;
                  }
                }

                var widget_url = 'http://easyod.com/widget/launch/' + $stateParams.id + '/' + $stateParams.type;

                var parameters = "";
                parameters += "?w="+width;
                parameters += "&h="+height;

                widget_url += parameters;

                $scope.HTMLblock = '<script language="javascript" type="text/javascript" src="' + widget_url + '"></script>';
            };
        }
    ]
);

angular.module('WeTalk').controller('ODApiGraphwidgetController',
    [
        '$scope',
        '$state',
        '$stateParams',
        'restService',
        function ($scope, $state, $stateParams, restService) {
            switch(parseInt($stateParams.type, 10)) {
                // Dispatching Events
                case 1:
                    showPieChart($stateParams.id);
                    break;
                case 2:
                    showDonutChart($stateParams.id);
                    break;
                case 3:
                    showBarChart($stateParams.id);
                    break;
                default:
                    break;
            }

            function showPieChart(id){
                restService.get(
                    {},
                    '/opendata/api/' + id,
                    function(data, status, headers, config) {

                        var graph_data = [];
                        $scope.graph_title = data.data.name;

                        _.each(data.data.sheets, function(d){

                            _.each(d.data, function(f){
                                var column_value;
                                var column_name;

                                _.each(f.fields, function(c){
                                    if (c.type == 'number') {
                                        column_value = parseFloat(c.value);
                                    }else if (c.type == 'text') {
                                        column_name = c.value;
                                    }
                                });
                                graph_data.push([column_name, column_value]);
                            });
                        });

                        var chart = c3.generate({
                            data: {
                                columns: graph_data,
                                type : 'pie',
                            },
                            tooltip: {
                                format: {
                                    value: function (value, ratio, id) {
                                        var format = id === 'data1' ? d3.format(',') : d3.format('');
                                        return format(value);
                                    }
                                }
                            }
                        });
                        
                    },
                    function(data, status, headers, config) {}
                );
            }

            function showDonutChart(id){
                restService.get(
                    {},
                    '/opendata/api/' + id,
                    function(data, status, headers, config) {

                        var graph_data = [];
                        $scope.graph_title = data.data.name;

                        _.each(data.data.sheets, function(d){

                            _.each(d.data, function(f){
                                var column_value;
                                var column_name;

                                _.each(f.fields, function(c){
                                    if (c.type == 'number') {
                                        column_value = parseFloat(c.value);
                                    }else if (c.type == 'text') {
                                        column_name = c.value;
                                    }
                                });
                                graph_data.push([column_name, column_value]);
                            });
                        });

                        var chart = c3.generate({
                            data: {
                                columns: graph_data,
                                type : 'donut',
                            },
                            tooltip: {
                                format: {
                                    value: function (value, ratio, id) {
                                        var format = id === 'data1' ? d3.format(',') : d3.format('');
                                        return format(value);
                                    }
                                }
                            }
                        });
                        
                    },
                    function(data, status, headers, config) {}
                );
            }

            function showBarChart(id){
                restService.get(
                    {},
                    '/opendata/api/' + id,
                    function(data, status, headers, config) {

                        var graph_data = [];
                        var graph_name = ['x'];
                        var bar_value_name_ok = false;
                        $scope.graph_title = data.data.name;

                        _.each(data.data.sheets, function(d){

                            _.each(d.data, function(f){
                                var bar_value;
                                var bar_value_name;
                                var bar_name;

                                _.each(f.fields, function(c){
                                    if (c.type == 'number') {
                                        bar_value = parseFloat(c.value);
                                        bar_value_name = c.name;
                                    }else if (c.type == 'text') {
                                        bar_name = c.value;
                                    }
                                });

                                if (bar_value_name_ok == false){
                                    graph_data.push(bar_value_name);
                                    bar_value_name_ok = true;
                                }
                                graph_data.push(bar_value);
                                graph_name.push(bar_name);
                            });
                        });

                    var chart = c3.generate({
                        data: {
                            x : 'x',
                            columns: [
                                graph_name,
                                graph_data,
                            ],
                            type: 'bar'
                        },
                        axis: {
                            x: {
                                type: 'category',
                                tick: {
                                    rotate: 0
                                },
                                height: 30
                            }
                        }
                    });
                },
                    function(data, status, headers, config) {}
                );
            }
        }
    ]
);


