// App main ng routes

angular.module('WeTalk').config(
    [
        '$stateProvider',
        '$urlRouterProvider',
        function($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            
            $stateProvider.
                state('raw_list', {
                    url: '/raw/list',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/opendata/parse",
                                controller: 'ODRawListController'
                            }
                    }
                }).
                state('raw_list_detail', {
                    url: '/:id/detail',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/opendata/raw/detail",
                                controller: 'ODRawController'
                            }
                    }
                }).
                state('api_list', {
                    url: '/api/list',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/opendata/api/list",
                                controller: 'ODApiListController'
                            }
                    }
                }).
                state('api_list_selector', {
                    url: '/api/graph/selector/:id',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/opendata/api/graph/selector",
                                controller: 'ODApiGraphController'
                            }
                    }
                }).
                state('api_widget_graph', {
                    url: '/api/widget/:id/:type',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/opendata/api/graph/widget",
                                controller: 'ODApiGraphwidgetController'
                            }
                    }
                }).
                state('api_list_graph', {
                    url: '/api/graph/:id/:type',
                    views: {
                        "main_content":
                            {
                                templateUrl: "/opendata/api/graph/representation",
                                controller: 'ODApiGraphRespresentationController'
                            }
                    }
                });
        }
    ]
);