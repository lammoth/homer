
/**
 * Chat events manager.
 */

var rek = require('rekuire');

module.exports = function(socket, io) {
    // Send a response when an url is parsed
    function urlParsed(url) {
        io.sockets.emit('urlAdded', url);
    }

    // // When a socket is connected
    // socket.on('userConnect', function (uid) {

    //     // Tells the others that have been connected.
    //     userConnected = {
    //         _id: socket.handshake.user._id,
    //         username: socket.handshake.user.username,
    //         image: socket.handshake.user.image
    //     };
    //     io.sockets.emit('userConnected', userConnected);
        
    //     // Tell me the users that are online.
    //     var data = [];
    //     for(var i in io.sockets.sockets)
    //         if(io.sockets.sockets[i].handshake.user.logged_in){
    //             var userData = {
    //                 _id: io.sockets.sockets[i].handshake.user._id,
    //                 username: io.sockets.sockets[i].handshake.user.username,
    //                 image: io.sockets.sockets[i].handshake.user.image
    //             };
    //             data.push(userData);
    //         }
    //     socket.emit('usersConnected', data);
           
    // });

    // // When create a new room
    // socket.on('newChatRoom', function (data) {
    //     passportSocketIo.filterSocketsByUser(io, function(user){
    //         if (user.logged_in)
    //             return data.users.indexOf(user._id.toString())+1;
    //     }).forEach(function(socket){
    //         messageModel
    //         .find({
    //             $or: [
    //                 {user_src: data.users[0], user_dst: data.users[1]},
    //                 {user_src: data.users[1], user_dst: data.users[0]}
    //             ]
    //         })
    //         .sort({created: -1})
    //         .limit(30)
    //         .exec(function(err, lastMessages) {
    //             var emitData = {name: data.name, uid:data.users, lastMessages: lastMessages};
    //             console.log(emitData);
    //             socket.emit('needSuscription', emitData);
                
    //         });
    //     });
    // });

    // // When someone sent a chat message
    // socket.on('sendChatMessage', function (data) {
    //     if (!data.room)
    //         return;
    //     var dot = data.room.indexOf('@');
    //     var user_a = data.room.substring(0,dot);
    //     var user_b = data.room.substring(dot+1, data.room.length);
        
    //     var user_src = socket.handshake.user._id.toString();
    //     var user_dst;
    //     if (user_a == user_src)
    //         user_dst = user_b;
    //     else if (user_b == user_src)
    //         user_dst = user_a;
    //     else
    //         return;

    //     var chatParams = {
    //         data: data,
    //         io: io,
    //         user_a: user_a,
    //         user_b: user_b
    //     };

    //     // TODO date UTC
    //     var messageData = {
    //         user_src: {
    //             value: user_src,
    //             type: 'objectid'
    //         },
    //         user_dst: {
    //             value: user_dst,
    //             type: 'objectid'
    //         },
    //         text: {
    //             value: data.message,
    //             type: 'text'
    //         },
    //         created: {
    //             value: new Date(),
    //             type: 'date'
    //         }
    //     };
    //     create_message(messageModel, messageData, chatParams, function(err) {
    //         if (!err)
    //             console.log('message saved');
    //         else
    //             console.log(err);
    //     });
       
        
    // });

    // // When someone is disconnected
    // socket.on('disconnect', function() {
    //     io.sockets.emit('userDisconnected', socket.handshake.user._id);
    // });
};