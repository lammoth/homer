/**
 * OpenData settings.
 */

var i18n = require("i18n");

// Module name
exports.name = i18n.__('OpenData places');

// Routes
exports.route_prefix = 'opendata';

// Root path
var modulePath = process.cwd() + '/modules/opendata';
exports.modulePath = modulePath;

// Paths
// Middlewares path
exports.middlewaresPath = modulePath + '/middlewares/';
// Models path
exports.modelsPath = modulePath + '/data/models/';
// Schemas path
exports.schemasPath = modulePath + '/data/schemas/';
// Views path
exports.viewsPath = modulePath + '/views/';
// Public path
exports.publicPath = modulePath + '/public/';
// Routes path
exports.routesPath = modulePath + '/routes/';
// Libs path
exports.libsPath = modulePath + '/libs/';
// Utils path
exports.utilsPath = modulePath + '/utils/';

// Site settings
exports.sections = [
    section = {
        name: i18n.__('Raw data list'),
        route: 'raw_list'
    },
    section = {
        name: i18n.__('Public API list'),
        route: 'api_list'
    }
];

exports.highlights = true;

// If module uses SocketIO
exports.hasSocketIO = true;