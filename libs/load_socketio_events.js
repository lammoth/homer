/**
 * @file load_socketio_events.js
 * @namespace Load Modules SocketIO events Middleware
 * @desc Middleware to load SocketIO events from modules
 */

var _ = require('underscore');
var rek = require('rekuire');
var settings = require('../settings');

module.exports = function loadSocketioEvents(socket, io, app) {
    // To load socketio events from modules
    // if (app.locals.modules) {
    //     _.each(app.locals.modules, function(module) {
    //         var mSettings = rek('modules/' + module.name + '/settings');
    //         if (mSettings.hasSocketIO) {
    //             var modSocketIOEvents = require(mSettings.utilsPath + 'socketio_events/index');
    //             modSocketIOEvents(socket, io);
    //             if (_.isEmpty(_.where(settings.modules_sio_events, {module: "opendata"}))) {
    //                 settings.modules_sio_events.push(
    //                     {
    //                         module: module.name,
    //                         sio_events: modSocketIOEvents,
    //                         socket: socket,
    //                         io: io
    //                     }
    //                 );
    //             }
    //         }
    //     });
    // }

    // To set last socket like main event (for broadcast purposes)
    settings.sio.io = io;
    settings.sio.socket = socket;
};
