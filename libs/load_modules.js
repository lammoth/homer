/**
 * @file load_modules.js
 * @namespace Load Modules Middleware
 * @desc Middleware to load modules
 */

var _ = require('underscore');
var rek = require('rekuire');
var settingModel = rek('data/models/admin/setting');

module.exports = function loadSelectedModules(app, path, modules_to_load) {
    var modules_routes = ['./routes'];
    app.locals.modules = [];

    /**
     * @desc  Creates a module db entry
     */
    function createModule(module) {
        var exists = false;
        settingModel.getModules(false, null, function(modules) {
            for (var index in modules) {
                if (modules[index].name == module) {
                    exists = true;
                    app.locals.modules.push(modules[index]);
                }
            }

            if (!exists) {
                settingModel.addModule(module, function() {
                    console.log('%s DB entry created', module);
                });
            }
        });
    }

    function loadModule(dir, module_name) {
        try {
            var mSettings = require(dir + '/settings');
            var moduleApp = require(dir + '/app');
            moduleApp.setModuleApp(app, dir, module_name);
            moduleApp.setModuleSections(app, mSettings);
            modules_routes.push(moduleApp.setModuleRoutes(dir));
            createModule(module_name);
        } catch (err) {
            console.log(err);
        }
    }

    for (var module in modules_to_load) {
        loadModule(path + modules_to_load[module], modules_to_load[module]);
    }

    if (modules_routes.length > 0) {
        app.set('modules_routes', modules_routes);
    }
        
};
