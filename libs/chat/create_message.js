
/**
 * Save an element
 */

var _ = require('underscore');
var rek = require('rekuire');
var createOperation = rek('libs/crud_operations/create');
var validator = rek('utils/form_validation');
var postValidatorTreatment = rek('utils/post_validator');
var passportSocketIo = require("passport.socketio");

function createObject(model, data, params, cb) {


    validator(data, function(err) {
        if (!err){
   
            createOperation(model, postValidatorTreatment(data), function(err){
                if (err) {
                    console.log(err);
                }
                else {
                    passportSocketIo.filterSocketsByUser(params.io, function(user){
                        if (user.logged_in)
                            return user._id.toString() === params.user_a || user._id.toString() === params.user_b;
                    }).forEach(function(socket){
                        socket.emit(params.data.room, params.data);
                    });
                    return cb();
                }
            });
    
        } else {
            return cb(err);
        }
    });

}

module.exports = createObject;
