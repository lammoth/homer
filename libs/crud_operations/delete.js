
/**
 * Update an element
 */


function deleteObject(model, query, cb) {

    model.remove(query, function (err) {
        if (err) return cb(err);
        cb();
    });
}

module.exports = deleteObject;
