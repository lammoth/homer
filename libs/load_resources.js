
/**
 * Middleware to insert custom theme
 */

var diveSync = require('diveSync'),
    path = require('path'),
    expressStatic = require('serve-static');

module.exports = function loadResources(app, route, module) {
    // Checks and creates locals arrays for styles
    if (!app.locals.styleFiles) {
        app.locals.styleFiles = [];
    }

    // Checks and creates locals arrays for javascripts
    if (!app.locals.jsFiles) {
        app.locals.jsFiles = [];
    }

    function findModules(dir) {
        // Parse route to add the static
        function checkAndInsert(file) {
            var extname = path.extname(file);
            // Checks if extension is .css
            if (extname === '.css') {
                if (!module) {
                    app.locals.styleFiles.push(
                        file.replace(process.cwd() + '/public', '')
                    );
                } else {
                    app.locals.styleFiles.push(
                        file.replace(process.cwd() + '/modules/' + module + '/public', '/' + module + '/public')
                    );
                }
            // Checks if extension is .js
            } else if (extname === '.js') {
                if (!module) {
                    app.locals.jsFiles.push(
                        file.replace(process.cwd() + '/public', '')
                    );
                } else {
                    app.locals.jsFiles.push(
                        file.replace(process.cwd() + '/modules/' + module + '/public', '/' + module + '/public')
                    );
                }
            }
        }

        // Dive into the dir to get .css and .js files
        diveSync(dir,
            {
              directories: false,
              all: false,
              recursive: true
            }, function(err, file) {
                if (err) throw err;
                checkAndInsert(file);
            }
        );
    }

    if (module) {
        app.use(
            '/' + module + '/public',
            expressStatic(
                path.join(process.cwd(),
                    'modules/' + module + '/public/'
                )
            )
        );
    }

    findModules(route);
};
